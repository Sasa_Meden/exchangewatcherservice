package com.EW.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
public class ExchangeWatcherService {//extends Thread {
	@SuppressWarnings("unused")
	private static Date LastDate;
	@SuppressWarnings("unused")
	private static Date TodayDate;
	private static int LastValute = 0;
	private static int TodayValute = 0;
	private static BigDecimal LastEUR;
	private static BigDecimal LastUSD;
	private static BigDecimal LastJPY;
	private static BigDecimal LastCAD;
	private static BigDecimal LastAUD;
	private static BigDecimal LastGBP;
	private static BigDecimal LastCZK;
	private static BigDecimal LastPLN;
	private static BigDecimal LastSEK;
	private static BigDecimal LastCHF;
	private static BigDecimal LastDKK;
	private static BigDecimal LastNOK;
	private static BigDecimal LastHUF;

	private static BigDecimal TodayEUR;
	private static BigDecimal TodayUSD;
	private static BigDecimal TodayJPY;
	private static BigDecimal TodayCAD;
	private static BigDecimal TodayAUD;
	private static BigDecimal TodayGBP;
	private static BigDecimal TodayCZK;
	private static BigDecimal TodayPLN;
	private static BigDecimal TodaySEK;
	private static BigDecimal TodayCHF;
	private static BigDecimal TodayDKK;
	private static BigDecimal TodayNOK;
	private static BigDecimal TodayHUF;

	private static int ParamEUR;
	private static float ParamEURPerc;
	private static int ParamUSD;
	private static float ParamUSDPerc;
	private static int ParamJPY;
	private static float ParamJPYPerc;
	private static int ParamCAD;
	private static float ParamCADPerc;
	private static int ParamAUD;
	private static float ParamAUDPerc;
	private static int ParamGBP;
	private static float ParamGBPPerc;
	private static int ParamCZK;
	private static float ParamCZKPerc;
	private static int ParamPLN;
	private static float ParamPLNPerc;
	private static int ParamSEK;
	private static float ParamSEKPerc;
	private static int ParamCHF;
	private static float ParamCHFPerc;
	private static int ParamDKK;
	private static float ParamDKKPerc;
	private static int ParamNOK;
	private static float ParamNOKPerc;
	private static int ParamHUF;
	private static float ParamHUFPerc;
	private static String ParamEmails;

	private static BigDecimal EURDiff;
	private static BigDecimal USDDiff;
	private static BigDecimal JPYDiff;
	private static BigDecimal CADDiff;
	private static BigDecimal AUDDiff;
	private static BigDecimal GBPDiff;
	private static BigDecimal CZKDiff;
	private static BigDecimal PLNDiff;
	private static BigDecimal SEKDiff;
	private static BigDecimal CHFDiff;
	private static BigDecimal DKKDiff;
	private static BigDecimal NOKDiff;
	private static BigDecimal HUFDiff;
	private static SimpleDateFormat formatToSend = new SimpleDateFormat("dd.MM.yyyy");
	private static SimpleDateFormat formatToCompare = new SimpleDateFormat("yyyy-MM-dd");

	private static int[] AllUsersID;

	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs = null;

	@SuppressWarnings("unused")
	private boolean urlInvalid = true;
	private static String GetCurrencies[];

	private static String QRYGetLastNumber = "SELECT num FROM hnbexchangewatcherschema.curr_values ORDER BY idcurr_values DESC LIMIT 1";
	private static String QRYCountUsers = "SELECT COUNT(iduser) FROM hnbexchangewatcherschema.user";
	private static String QRYGetAllUsers = "SELECT iduser FROM hnbexchangewatcherschema.user";

	private static String QRYGetLastValues = "SELECT date,eur,usd,jpy,cad,aud,gbp,czk,pln,sek,chf,dkk,nok,huf,num FROM "
			+ "hnbexchangewatcherschema.curr_values ORDER BY idcurr_values DESC LIMIT 1";

	private static String QRYInsert = "INSERT INTO `hnbexchangewatcherschema`.`curr_values` "
			+ "(`date`, `eur`, `usd`, `jpy`, `cad`, `aud`, `gbp`, `czk`, `pln`, `sek`, `chf`, `dkk`, `nok`, `huf`, `num`) VALUES ";

	private static String QRYGetParams = "SELECT eur,eur_percentage,usd,usd_percentage,jpy,jpy_percentage,cad,cad_percentage,"
			+ "aud,aud_percentage,gbp,gbp_percentage,czk,czk_percentage,pln,pln_percentage,sek,sek_percentage,"
			+ "chf,chf_percentage,dkk,dkk_percentage,nok,nok_percentage,huf,huf_percentage,e_mails"
			+ " FROM hnbexchangewatcherschema.user_settings WHERE iduser = @idUser";

	public static void main(String[] args) {
		//get();
		// TODO Auto-generated method stub
		getLastNumber();
		if(LastValute < TodayValute){
			get();
			getLast();
			CountUsers();
			GetAllUsers();
			CalculateDiff();
			GetParams();
				
		}else{
			//SendErrorLog("NoChange : ","\n"+TodayValute);
		}
		//set();
		//SendErrorLog("","");

	}
	public static void SaveErrorLog(String function,String error){

	}
	@SuppressWarnings("unused")
	public static void SendErrorLog(String function,String error){
		Date msgDate = new Date();
		String tosend="";
		String subject;
		String message;
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		// Sender's email ID needs to be mentioned
		String from = "hnbexchange.watcher@gmail.com";
		// Assuming you are sending email from localhost
		String host = "localhost";
		// Get system properties
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", "smtp.gmail.com");
		properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		properties.setProperty("mail.smtp.socketFactory.fallback", "false");
		properties.setProperty("mail.smtp.port", "465");
		properties.setProperty("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.debug", "true");
		properties.put("mail.store.protocol", "pop3");
		properties.put("mail.transport.protocol", "smtp");
		final String username = from;
		final String password = "hnb_Exchange1020304050";
		subject = "Exchange Watcher - ERROR " +msgDate;
		message = msgDate +"\n"+function+"\n"+error;
		try{
			Session session = Session.getDefaultInstance(properties,new Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}});

			// -- Create a new message --
			Message msg = new MimeMessage(session);

			// -- Set the FROM and TO fields --
			msg.setFrom(new InternetAddress(from));
			msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse("sasa.meden@gmail.com",false));
			msg.setSubject(subject);
			msg.setText(message);
			msg.setSentDate(new Date());
			Transport.send(msg);
			System.out.println("Message sent.");


		}catch (MessagingException e){ 
			//System.out.println("Error cause: " + e);
			SaveErrorLog("SendErrorLog()\nMessagingException",e.toString());
		}

	}

	public static void CompareAndSend(){
		String strToSend = "";
		boolean up;
		try{
			///EUR
			if(ParamEUR == 1){
				if(ParamEURPerc == 0.0f){
					strToSend +="\nEUR: "+TodayEUR;
				}else{
					
					if(EURDiff.compareTo(new BigDecimal("0"))==-1){
						EURDiff = EURDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(EURDiff.compareTo(new BigDecimal(Float.toString(ParamEURPerc)))==1||EURDiff.compareTo(new BigDecimal(Float.toString(ParamEURPerc)))==0){
						if(up){
							strToSend +="\nEUR: "+TodayEUR + " UP "+EURDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nEUR: "+TodayEUR + " DOWN "+EURDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}


			///USD
			if(ParamUSD == 1){
				if(ParamUSDPerc == 0.0f){
					strToSend +="\nUSD: "+TodayUSD;
				}else{
					
					if(USDDiff.compareTo(new BigDecimal("0"))==-1){
						USDDiff = USDDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(USDDiff.compareTo(new BigDecimal(Float.toString(ParamUSDPerc)))==1||USDDiff.compareTo(new BigDecimal(Float.toString(ParamUSDPerc)))==0){
						if(up){
							strToSend +="\nUSD: "+TodayUSD + " UP "+USDDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nUSD: "+TodayUSD + " DOWN "+USDDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}

			///JPY
			if(ParamJPY == 1){
				if(ParamJPYPerc == 0.0f){
					strToSend +="\nJPY: "+TodayJPY;
				}else{
					
					if(JPYDiff.compareTo(new BigDecimal("0"))==-1){
						JPYDiff = JPYDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(JPYDiff.compareTo(new BigDecimal(Float.toString(ParamJPYPerc)))==1||JPYDiff.compareTo(new BigDecimal(Float.toString(ParamJPYPerc)))==0){
						if(up){
							strToSend +="\nJPY: "+TodayJPY + " UP "+JPYDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nJPY: "+TodayJPY + " DOWN "+JPYDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}

			///CAD
			if(ParamCAD == 1){
				if(ParamCADPerc == 0.0f){
					strToSend +="\nCAD: "+TodayCAD;
				}else{
					
					if(CADDiff.compareTo(new BigDecimal("0"))==-1){
						CADDiff = CADDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(CADDiff.compareTo(new BigDecimal(Float.toString(ParamCADPerc)))==1||CADDiff.compareTo(new BigDecimal(Float.toString(ParamCADPerc)))==0){
						if(up){
							strToSend +="\nCAD: "+TodayCAD + " UP "+CADDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nCAD: "+TodayCAD + " DOWN "+CADDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}

			///AUD
			if(ParamAUD == 1){
				if(ParamAUDPerc == 0.0f){
					strToSend +="\nAUD: "+TodayAUD;
				}else{
					
					if(AUDDiff.compareTo(new BigDecimal("0"))==-1){
						AUDDiff = AUDDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(AUDDiff.compareTo(new BigDecimal(Float.toString(ParamAUDPerc)))==1||AUDDiff.compareTo(new BigDecimal(Float.toString(ParamAUDPerc)))==0){
						if(up){
							strToSend +="\nAUD: "+TodayAUD + " UP "+AUDDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nAUD: "+TodayAUD + " DOWN "+AUDDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}

			///GBP
			if(ParamGBP == 1){
				if(ParamGBPPerc == 0.0f){
					strToSend +="\nGBP: "+TodayGBP;
				}else{
					
					if(GBPDiff.compareTo(new BigDecimal("0"))==-1){
						GBPDiff = GBPDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(GBPDiff.compareTo(new BigDecimal(Float.toString(ParamGBPPerc)))==1||GBPDiff.compareTo(new BigDecimal(Float.toString(ParamGBPPerc)))==0){
						if(up){
							strToSend +="\nGBP: "+TodayGBP + " UP "+GBPDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nGBP: "+TodayGBP + " DOWN "+GBPDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}

			///CZK
			if(ParamCZK == 1){
				if(ParamCZKPerc == 0.0f){
					strToSend +="\nCZK: "+TodayCZK;
				}else{
					
					if(CZKDiff.compareTo(new BigDecimal("0"))==-1){
						CZKDiff = CZKDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(CZKDiff.compareTo(new BigDecimal(Float.toString(ParamCZKPerc)))==1||CZKDiff.compareTo(new BigDecimal(Float.toString(ParamCZKPerc)))==0){
						if(up){
							strToSend +="\nCZK: "+TodayCZK + " UP "+CZKDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nCZK: "+TodayCZK + " DOWN "+CZKDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}

			///PLN
			if(ParamPLN == 1){
				if(ParamPLNPerc == 0.0f){
					strToSend +="\nPLN: "+TodayPLN;
				}else{
					
					if(PLNDiff.compareTo(new BigDecimal("0"))==-1){
						PLNDiff = PLNDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(PLNDiff.compareTo(new BigDecimal(Float.toString(ParamPLNPerc)))==1||PLNDiff.compareTo(new BigDecimal(Float.toString(ParamPLNPerc)))==0){
						if(up){
							strToSend +="\nPLN: "+TodayPLN + " UP "+PLNDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nPLN: "+TodayPLN + " DOWN "+PLNDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}

			///SEK
			if(ParamSEK == 1){
				if(ParamSEKPerc == 0.0f){
					strToSend +="\nSEK: "+TodaySEK;
				}else{
					
					if(SEKDiff.compareTo(new BigDecimal("0"))==-1){
						SEKDiff = SEKDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(SEKDiff.compareTo(new BigDecimal(Float.toString(ParamSEKPerc)))==1||SEKDiff.compareTo(new BigDecimal(Float.toString(ParamSEKPerc)))==0){
						if(up){
							strToSend +="\nSEK: "+TodaySEK + " UP "+SEKDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nSEK: "+TodaySEK + " DOWN "+SEKDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}

			///CHF
			if(ParamCHF == 1){
				if(ParamCHFPerc == 0.0f){
					strToSend +="\nCHF: "+TodayCHF;
				}else{
					
					if(CHFDiff.compareTo(new BigDecimal("0"))==-1){
						CHFDiff = CHFDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(CHFDiff.compareTo(new BigDecimal(Float.toString(ParamCHFPerc)))==1||CHFDiff.compareTo(new BigDecimal(Float.toString(ParamCHFPerc)))==0){
						if(up){
							strToSend +="\nCHF: "+TodayCHF + " UP "+CHFDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nCHF: "+TodayCHF + " DOWN "+CHFDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}
			///DKK
			if(ParamDKK == 1){
				if(ParamDKKPerc == 0.0f){
					strToSend +="\nDKK: "+TodayDKK;
				}else{
					
					if(DKKDiff.compareTo(new BigDecimal("0"))==-1){
						DKKDiff = DKKDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(DKKDiff.compareTo(new BigDecimal(Float.toString(ParamDKKPerc)))==1||DKKDiff.compareTo(new BigDecimal(Float.toString(ParamDKKPerc)))==0){
						if(up){
							strToSend +="\nDKK: "+TodayDKK + " UP "+DKKDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nDKK: "+TodayDKK + " DOWN "+DKKDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}
			///NOK
			if(ParamNOK == 1){
				if(ParamNOKPerc == 0.0f){
					strToSend +="\nNOK: "+TodayNOK;
				}else{
					
					if(NOKDiff.compareTo(new BigDecimal("0"))==-1){
						NOKDiff = NOKDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(NOKDiff.compareTo(new BigDecimal(Float.toString(ParamNOKPerc)))==1||NOKDiff.compareTo(new BigDecimal(Float.toString(ParamNOKPerc)))==0){
						if(up){
							strToSend +="\nNOK: "+TodayNOK + " UP "+NOKDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nNOK: "+TodayNOK + " DOWN "+NOKDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}
			///HUF
			if(ParamHUF == 1){
				if(ParamHUFPerc == 0.0f){
					strToSend +="\nHUF: "+TodayHUF;
				}else{
					
					if(HUFDiff.compareTo(new BigDecimal("0"))==-1){
						HUFDiff = HUFDiff.multiply(new BigDecimal("-1"));
						//System.out.println(result.toString());
						up = false;
					}else{
						up = true;
					}
					if(HUFDiff.compareTo(new BigDecimal(Float.toString(ParamHUFPerc)))==1||HUFDiff.compareTo(new BigDecimal(Float.toString(ParamHUFPerc)))==0){
						if(up){
							strToSend +="\nHUF: "+TodayHUF + " UP "+HUFDiff+"%";
							//System.out.println(send);
						}else{
							strToSend +="\nHUF: "+TodayHUF + " DOWN "+HUFDiff+"%";
							//System.out.println(send);
						}
					}
				}
			}
			if(!strToSend.equals("")){
				//SEND EMAIL
				SendEmail(strToSend);

			}
		}catch(Exception e){
			SendErrorLog("CompareAndSend()",e.toString());
		}
		//System.out.println("SEND//////////////////////////////////////////////////");
		//System.out.println(strToSend);

	}
	@SuppressWarnings("unused")
	public static void SendEmail(String strSend){
		String msgDate = formatToSend.format(new Date());
		String tosend="";
		String temp[];
		String subject;
		String message;
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		// Sender's email ID needs to be mentioned
		String from = "hnbexchange.watcher@gmail.com";
		// Assuming you are sending email from localhost
		String host = "localhost";
		// Get system properties
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", "smtp.gmail.com");
		properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		properties.setProperty("mail.smtp.socketFactory.fallback", "false");
		properties.setProperty("mail.smtp.port", "465");
		properties.setProperty("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.debug", "true");
		properties.put("mail.store.protocol", "pop3");
		properties.put("mail.transport.protocol", "smtp");
		final String username = from;
		final String password = "hnb_Exchange1020304050";
		temp = ParamEmails.split(":");
		subject = "Exchange Watcher - Currencie Alert " +msgDate;
		message = msgDate +"\n"+"Currencie values\n";
		message += strSend;



		try{
			for(int i = 0; i< temp.length;i++){
				Session session = Session.getDefaultInstance(properties,new Authenticator(){
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}});

				// -- Create a new message --
				Message msg = new MimeMessage(session);

				// -- Set the FROM and TO fields --
				msg.setFrom(new InternetAddress(from));
				msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(temp[i],false));
				msg.setSubject(subject);
				msg.setText(message);
				msg.setSentDate(new Date());
				Transport.send(msg);
				//System.out.println("Message sent.");
			}

		}catch (MessagingException e){ 
			SaveErrorLog("SendEmail()\nMessagingException", e.toString());
			//System.out.println("Error cause: " + e);
		}


	}


	public static void CalculateDiff(){
		try{

			EURDiff = TodayEUR.subtract(LastEUR);
			EURDiff = EURDiff.divide(TodayEUR, 6, RoundingMode.CEILING);
			EURDiff = EURDiff.multiply(new BigDecimal("100"));
			EURDiff = EURDiff.setScale(2, RoundingMode.CEILING);

			USDDiff = TodayUSD.subtract(LastUSD);
			USDDiff = USDDiff.divide(TodayUSD, 6, RoundingMode.CEILING);
			USDDiff = USDDiff.multiply(new BigDecimal("100"));
			USDDiff = USDDiff.setScale(2, RoundingMode.CEILING);

			JPYDiff = TodayJPY.subtract(LastJPY);
			JPYDiff = JPYDiff.divide(TodayJPY, 6, RoundingMode.CEILING);
			JPYDiff = JPYDiff.multiply(new BigDecimal("100"));
			JPYDiff = JPYDiff.setScale(2, RoundingMode.CEILING);

			CADDiff = TodayCAD.subtract(LastCAD);
			CADDiff = CADDiff.divide(TodayCAD, 6, RoundingMode.CEILING);
			CADDiff = CADDiff.multiply(new BigDecimal("100"));
			CADDiff = CADDiff.setScale(2, RoundingMode.CEILING);

			AUDDiff = TodayAUD.subtract(LastAUD);
			AUDDiff = AUDDiff.divide(TodayAUD, 6, RoundingMode.CEILING);
			AUDDiff = AUDDiff.multiply(new BigDecimal("100"));
			AUDDiff = AUDDiff.setScale(2, RoundingMode.CEILING);

			GBPDiff = TodayGBP.subtract(LastGBP);
			GBPDiff = GBPDiff.divide(TodayGBP, 6, RoundingMode.CEILING);
			GBPDiff = GBPDiff.multiply(new BigDecimal("100"));
			GBPDiff = GBPDiff.setScale(2, RoundingMode.CEILING);

			CZKDiff = TodayCZK.subtract(LastCZK);
			CZKDiff = CZKDiff.divide(TodayCZK, 6, RoundingMode.CEILING);
			CZKDiff = CZKDiff.multiply(new BigDecimal("100"));
			CZKDiff = CZKDiff.setScale(2, RoundingMode.CEILING);

			PLNDiff = TodayPLN.subtract(LastPLN);
			PLNDiff = PLNDiff.divide(TodayPLN, 6, RoundingMode.CEILING);
			PLNDiff = PLNDiff.multiply(new BigDecimal("100"));
			PLNDiff = PLNDiff.setScale(2, RoundingMode.CEILING);

			SEKDiff = TodaySEK.subtract(LastSEK);
			SEKDiff = SEKDiff.divide(TodaySEK, 6, RoundingMode.CEILING);
			SEKDiff = SEKDiff.multiply(new BigDecimal("100"));
			SEKDiff = SEKDiff.setScale(2, RoundingMode.CEILING);

			CHFDiff = TodayCHF.subtract(LastCHF);
			CHFDiff = CHFDiff.divide(TodayCHF, 6, RoundingMode.CEILING);
			CHFDiff = CHFDiff.multiply(new BigDecimal("100"));
			CHFDiff = CHFDiff.setScale(2, RoundingMode.CEILING);

			DKKDiff = TodayDKK.subtract(LastDKK);
			DKKDiff = DKKDiff.divide(TodayDKK, 6, RoundingMode.CEILING);
			DKKDiff = DKKDiff.multiply(new BigDecimal("100"));
			DKKDiff = DKKDiff.setScale(2, RoundingMode.CEILING);

			NOKDiff = TodayNOK.subtract(LastNOK);
			NOKDiff = NOKDiff.divide(TodayNOK, 6, RoundingMode.CEILING);
			NOKDiff = NOKDiff.multiply(new BigDecimal("100"));
			NOKDiff = NOKDiff.setScale(2, RoundingMode.CEILING);

			HUFDiff = TodayHUF.subtract(LastHUF);
			HUFDiff = HUFDiff.divide(TodayHUF, 6, RoundingMode.CEILING);
			HUFDiff = HUFDiff.multiply(new BigDecimal("100"));
			HUFDiff = HUFDiff.setScale(2, RoundingMode.CEILING);

		/*	System.out.println("DIFF//////////////////////////////////////////////////////////");

		System.out.println("EUR:"+EURDiff+"\n"+"USD:"+USDDiff+"\n"+"JPY:"+JPYDiff+"\n"+
				"CAD:"+CADDiff+"\n"+"AUD:"+AUDDiff+"\n"+"GBP:"+GBPDiff+"\n"+"CZK:"+CZKDiff+"\n"+
				"PLN:"+PLNDiff+"\n"+"SEK:"+SEKDiff+"\n"+"CHF:"+CHFDiff+"\n"+"DKK:"+DKKDiff+"\n"+
				"NOK:"+NOKDiff+"\n"+"HUF:"+HUFDiff);*/
		}catch (Exception e) {
			SendErrorLog("CalculateDiff()",e.toString());
			//e.printStackTrace();
		} 

	}

	public static void CountUsers(){
		int count = 0;
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
			String connectionUser = "root";
			String connectionPassword = "Code_Cons_Rulez1";
			conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = (Statement) conn.createStatement();
			rs = stmt.executeQuery(QRYCountUsers);
			while (rs.next()) {
				count = rs.getInt(1);
			}
			AllUsersID = new int[count];
		} catch (Exception e) {
			SendErrorLog("CountUsers()",e.toString());
			//e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				SendErrorLog("CountUsers()\nSQLException",e.toString());
				//e.printStackTrace();
			}
			try { if (stmt != null) stmt.close(); } catch (SQLException e) {
				SendErrorLog("CountUsers()\nSQLException",e.toString());
				//e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				SendErrorLog("CountUsers()\nSQLException",e.toString());
				//e.printStackTrace();
			}
		}	

	}
	public static void GetAllUsers(){
		int i = 0;
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
			String connectionUser = "root";
			String connectionPassword = "Code_Cons_Rulez1";
			conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = (Statement) conn.createStatement();
			rs = stmt.executeQuery(QRYGetAllUsers);
			while (rs.next()) {
				AllUsersID[i] = rs.getInt(1);
				i++;
			}

		} catch (Exception e) {
			SendErrorLog("GetAllUsers()",e.toString());
			//e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				SendErrorLog("GetAllUsers()\nSQLException",e.toString());
				//e.printStackTrace();
			}
			try { if (stmt != null) stmt.close(); } catch (SQLException e) {
				SendErrorLog("GetAllUsers()\nSQLException",e.toString());
				//e.printStackTrace(); 
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				SendErrorLog("GetAllUsers()\nSQLException",e.toString());
				//e.printStackTrace(); 
			}
		}	
		/*for(int j = 0; j < AllUsersID.length;j++){
			System.out.println(AllUsersID[j]+"\n");

		}*/

	}
	public static void GetParams(){
		for(int i = 0;i < AllUsersID.length;i++){
			try{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
				String connectionUser = "root";
				String connectionPassword = "Code_Cons_Rulez1";
				conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
				stmt = (Statement) conn.createStatement();
				rs = stmt.executeQuery(param(QRYGetParams,AllUsersID[i]));
				while (rs.next()) {
					ParamEUR = rs.getInt(1);
					ParamEURPerc = rs.getFloat(2);
					ParamUSD = rs.getInt(3);
					ParamUSDPerc =rs.getFloat(4);
					ParamJPY = rs.getInt(5);
					ParamJPYPerc = rs.getFloat(6);
					ParamCAD = rs.getInt(7);
					ParamCADPerc = rs.getFloat(8);
					ParamAUD = rs.getInt(9);
					ParamAUDPerc = rs.getFloat(10);
					ParamGBP = rs.getInt(11);
					ParamGBPPerc = rs.getFloat(12);
					ParamCZK = rs.getInt(13);
					ParamCZKPerc = rs.getFloat(14);
					ParamPLN = rs.getInt(15);
					ParamPLNPerc = rs.getFloat(16);
					ParamSEK = rs.getInt(17);
					ParamSEKPerc = rs.getFloat(18);
					ParamCHF = rs.getInt(19);
					ParamCHFPerc = rs.getFloat(20);
					ParamDKK = rs.getInt(21);
					ParamDKKPerc = rs.getFloat(22);
					ParamNOK = rs.getInt(23);
					ParamNOKPerc = rs.getFloat(24);
					ParamHUF = rs.getInt(25);
					ParamHUFPerc = rs.getFloat(26);
					ParamEmails = rs.getString(27);

				}

			} catch (Exception e) {
				SendErrorLog("GetParams()",e.toString());
				//e.printStackTrace();
			} finally {
				try { if (rs != null) rs.close(); } catch (SQLException e) {
					SendErrorLog("GetParams()\nSQLException",e.toString());
					//e.printStackTrace(); 
				}
				try { if (stmt != null) stmt.close(); } catch (SQLException e) {
					SendErrorLog("GetParams()\nSQLException",e.toString());
					//e.printStackTrace();
				}
				try { if (conn != null) conn.close(); } catch (SQLException e) {
					SendErrorLog("GetParams()\nSQLException",e.toString());
					//e.printStackTrace();
				}
				CompareAndSend();
			}	

			////COMPARE AND SEND
			

			/*System.out.println("EUR:"+ParamEUR+":"+ParamEURPerc+"\n"+"USD:"+ParamUSD+":"+ParamUSDPerc+"\n"+"JPY:"+ParamJPY+":"+ParamJPYPerc+"\n"+
					"CAD:"+ParamCAD+":"+ParamCADPerc+"\n"+"AUD:"+ParamAUD+":"+ParamAUDPerc+"\n"+"GBP:"+ParamGBP+":"+ParamGBPPerc+"\n"+"CZK:"+ParamCZK+":"+ParamCZKPerc+"\n"+
					"PLN:"+ParamPLN+":"+ParamPLNPerc+"\n"+"SEK:"+ParamSEK+":"+ParamSEKPerc+"\n"+"CHF:"+ParamCHF+":"+ParamCHFPerc+"\n"+"DKK:"+ParamDKK+":"+ParamDKKPerc+"\n"+
					"NOK:"+ParamNOK+":"+ParamNOKPerc+"\n"+"HUF:"+ParamHUF+":"+ParamHUFPerc+"\n"+ParamEmails);*/

		}
		set();
	}
	public static void getLast(){
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
			String connectionUser = "root";
			String connectionPassword = "Code_Cons_Rulez1";
			conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = (Statement) conn.createStatement();
			rs = stmt.executeQuery(QRYGetLastValues);
			while (rs.next()) {
				LastDate = rs.getDate(1);
				LastEUR = rs.getBigDecimal(2);
				LastUSD = rs.getBigDecimal(3);
				LastJPY = rs.getBigDecimal(4);
				LastCAD = rs.getBigDecimal(5);
				LastAUD = rs.getBigDecimal(6);
				LastGBP = rs.getBigDecimal(7);
				LastCZK = rs.getBigDecimal(8);
				LastPLN = rs.getBigDecimal(9);
				LastSEK = rs.getBigDecimal(10);
				LastCHF = rs.getBigDecimal(11);
				LastDKK = rs.getBigDecimal(12);
				LastNOK = rs.getBigDecimal(13);
				LastHUF = rs.getBigDecimal(14);
				LastValute = rs.getInt(15);
			}

		} catch (Exception e) {
			SendErrorLog("getLast()",e.toString());
			//e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { 
				SendErrorLog("getLast()\nSQLException",e.toString());
				//e.printStackTrace();
			}
			try { if (stmt != null) stmt.close(); } catch (SQLException e) {
				SendErrorLog("getLast()\nSQLException",e.toString());
				//e.printStackTrace(); 
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				SendErrorLog("getLast()\nSQLException",e.toString());
				//e.printStackTrace();
			}
		}	
		/*	System.out.println("//////////////////////////////////////////////////////////");
		System.out.println("Date:"+LastDate+"\n"+"EUR:"+LastEUR+"\n"+"USD:"+LastUSD+"\n"+"JPY:"+LastJPY+"\n"+
				"CAD:"+LastCAD+"\n"+"AUD:"+LastAUD+"\n"+"GBP:"+LastGBP+"\n"+"CZK:"+LastCZK+"\n"+
				"PLN:"+LastPLN+"\n"+"SEK:"+LastSEK+"\n"+"CHF:"+LastCHF+"\n"+"DKK:"+LastDKK+"\n"+
				"NOK:"+LastNOK+"\n"+"HUF:"+LastHUF);*/


	}
	
	@SuppressWarnings("unused")
	public static void set(){
		
		try{
			int i = 0;
			Date today = new Date();
			String dToday = formatToCompare.format(today);
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
			String connectionUser = "root";
			String connectionPassword = "Code_Cons_Rulez1";
			conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = (Statement) conn.createStatement();
			QRYInsert = QRYInsert +="('"+dToday+"', '"+TodayEUR+"', '"+TodayUSD+"', '"+TodayJPY+"', '"+TodayCAD+"', '"+TodayAUD+"',"
									+"'"+TodayGBP+"', '"+TodayCZK+"', '"+TodayPLN+"', '"+TodaySEK+"', '"+TodayCHF+"', '"+TodayDKK+"',"
									+"'"+TodayNOK+"', '"+TodayHUF+"', '"+TodayValute+"')";
			i = stmt.executeUpdate(QRYInsert);


		} catch (Exception e) {
			SendErrorLog("set()",e.toString());
			//e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				SendErrorLog("set()\nSQLException",e.toString());
				//e.printStackTrace(); 
			}
			try { if (stmt != null) stmt.close(); } catch (SQLException e) {
				SendErrorLog("set()\nSQLException",e.toString());
				//e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				SendErrorLog("set()\nSQLException",e.toString());
				//e.printStackTrace();
			}
		}	
		//System.out.println(""+i);

	}
	public static void getLastNumber(){
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
			String connectionUser = "root";
			String connectionPassword = "Code_Cons_Rulez1";
			conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = (Statement) conn.createStatement();
			rs = stmt.executeQuery(QRYGetLastNumber);
			while (rs.next()) {
				LastValute = rs.getInt(1);
			}

		} catch (Exception e) {
			SendErrorLog("getLastNumber()",e.toString());
			//e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				SendErrorLog("getLastNumber()\nSQLException",e.toString());
				//e.printStackTrace(); 
			}
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { 
				SendErrorLog("getLastNumber()\nSQLException",e.toString());
				//e.printStackTrace(); 
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				SendErrorLog("getLastNumber()\nSQLException",e.toString());
				//e.printStackTrace(); 
			}
			getTodayNumber();
		}	
	}
	public static void getTodayNumber(){
		URL url;
		String line = null;
		int i = 0;
		try {
			url = new URL("http://www.hnb.hr/hnb-tecajna-lista-portlet/rest/tecajn/getformatedrecords.dat");
			URLConnection con = url.openConnection();
			InputStream is = con.getInputStream();
			BufferedReader br_input = new BufferedReader(new InputStreamReader(is));
			while ((line = br_input.readLine()) != null) {
				if(i==0 && line.length()< 30){
					TodayValute = Integer.parseInt(line.substring(0, 3));
					//System.out.println(TodayValute+"");
				}
				i++;
			}
		} catch (MalformedURLException e) {
			SendErrorLog("getTodayNumber()\nMalformedURLException",e.toString());
			//e.printStackTrace();
		} catch (IOException e) {
			SendErrorLog("getTodayNumber()\nIOException",e.toString());
			//e.printStackTrace();
		}
	}
	public static void get(){
		URL url;
		String line = null;
		String temp="0.0";
		int i = 0;
		try {
			url = new URL("http://www.hnb.hr/hnb-tecajna-lista-portlet/rest/tecajn/getformatedrecords.dat");
			URLConnection con = url.openConnection();
			InputStream is = con.getInputStream();
			BufferedReader br_input = new BufferedReader(new InputStreamReader(is));
			while ((line = br_input.readLine()) != null) {
				//System.out.println(line.length());
				if(i==0 && line.length()< 30){
					TodayValute = Integer.parseInt(line.substring(0, 3));
					//System.out.println(TodayValute+"");
				}
				i++;
				if(line.contains("EUR")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayEUR = new BigDecimal(temp);
				}
				if(line.contains("USD")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayUSD = new BigDecimal(temp);
				}
				if(line.contains("JPY")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayJPY = new BigDecimal(temp);
				}
				if(line.contains("CAD")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayCAD = new BigDecimal(temp);
				}
				if(line.contains("AUD")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayAUD = new BigDecimal(temp);
				}
				if(line.contains("GBP")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayGBP = new BigDecimal(temp);
				}
				if(line.contains("CZK")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayCZK = new BigDecimal(temp);
				}
				if(line.contains("PLN")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayPLN = new BigDecimal(temp);
				}
				if(line.contains("SEK")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodaySEK = new BigDecimal(temp);
				}
				if(line.contains("CHF")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayCHF = new BigDecimal(temp);
				}
				if(line.contains("DKK")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayDKK = new BigDecimal(temp);
				}
				if(line.contains("NOK")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayNOK = new BigDecimal(temp);
				}
				if(line.contains("HUF")){
					GetCurrencies = line.split("\\s+");
					temp = GetCurrencies[2].replace(",", ".");
					TodayHUF = new BigDecimal(temp);
				}
			}
		} catch (MalformedURLException e) {
			SendErrorLog("get()\nMalformedURLException",e.toString());
			//e.printStackTrace();
		} catch (IOException e) {
			SendErrorLog("get()\nIOException",e.toString());
			//e.printStackTrace();
		}
		catch(Exception e){
			SendErrorLog("get()",e.toString());
		}
		/*	System.out.println("//////////////////////////////////////////////////////////");
		System.out.println(TodayEUR+"\n"+TodayUSD+"\n"+TodayJPY+"\n"+TodayCAD+"\n"+TodayAUD+"\n"+TodayGBP+"\n"+TodayCZK+"\n"+TodayPLN
				+"\n"+TodaySEK+"\n"+TodayCHF+"\n"+TodayDKK+"\n"+TodayNOK+"\n"+TodayHUF);*/

	}
	public static String param(String Qry,int param){
		String qry;
		qry = Qry;
		qry = qry.replace("@idUser", param+"");
		//System.out.println(qry);
		return qry;
	}
}
